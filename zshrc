# ben .zshrc

# options
setopt correct # correct mistakes
setopt extendedglob # Extended globbing. Allows using regular expressions with *
setopt nocaseglob # Case insensitive globbing
#unsetopt nomatch # Passes the command as is instead of reporting pattern matching failure see Chrysostomus/manjaro-zsh-config#14
setopt rcexpandparam  # Array expension with parameters
setopt nocheckjobs # Don't warn about running processes when exiting
setopt numericglobsort # Sort filenames numerically when it makes sense
#setopt nobeep # No beep
setopt appendhistory # Immediately append history instead of overwriting
setopt histignorealldups # If a new command is a duplicate, remove the older one
setopt prompt_subst # enable substitution for prompt
#setopt incappendhistory
setopt share_history

# zsh tab-completion
zstyle ':completion:*' menu select # highlight selected tab completion
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' # Case insensitive tab completion
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}" # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true # automatically find new executables in path 

# Speed up completions
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.cache/zsh


# theme
autoload -Uz compinit colors
compinit
colors


# plugins
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh


# keybinds
bindkey -e # emacs-style keybinds
bindkey '^[[H' beginning-of-line # home
bindkey '^[[F' end-of-line # end
bindkey '^[[2~' overwrite-mode # insert
bindkey '^[[3~' delete-char # delete
bindkey '^[[C'  forward-char # right key
bindkey '^[[D'  backward-char # left key
bindkey '^[[1;5C' forward-word # ctrl+right
bindkey '^[[1;5D' backward-word # ctrl+left
bindkey '^H' backward-kill-word # ctrl+backspace
bindkey '^[[3;5~' kill-word # ctrl+delete
bindkey '^[[A' history-substring-search-up # up key
bindkey '^[[B' history-substring-search-down # down key


# Colour man pages
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r


# History
HISTFILE=~/.histfile
HISTSIZE=10000 # lines in memory
SAVEHIST=20000 # lines in histfile


# prompt
PROMPT="%{$fg[green]%}[%n@%M %{$fg[cyan]%}%~%{$fg[green]%}]%{$reset_color%}%(!.#.$) "
RPROMPT="%{$fg[red]%} %(?..[%?])"


# env preferences
EDITOR=vim
PAGER=less
TERMINAL=kitty


# Aliases
if [[ -f ~/.zaliases ]] ; then
	. ~/.zaliases ; fi

# greeting
# format date
day=$(date +%e)
case ${day} in
    1?) day=${day}th ;;
    *1) day=${day}st ;;
    *2) day=${day}nd ;;
    *3) day=${day}rd ;;
    *) day=${day}th ;;
esac

print -P $fg[cyan]$USER@$HOST $fg[blue]$(uname -srm) %063F
date +"%A, ${day} of %B %Y - %H:%M" ; echo -n $reset_color
